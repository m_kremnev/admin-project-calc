import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/Home';
import Algebra from '@/components/Algebra/Algebra';
import Geometria from '@/components/Geometria/Geometria';
import Biology from '@/components/Biology/Biology';
import Chemistry from '@/components/Chemistry/Chemistry';
import Economics from '@/components/Economics/Economics';
import Physics from '@/components/Physics/Physics';

Vue.use(VueRouter);

const routes = [
	{
		path: '',
		name: 'home',
		component: Home
	},
	{
		path: '/algebra',
		name: 'algebra',
		component: Algebra
	},
	{
		path: '/geometria',
		name: 'geometria',
		component: Geometria
	},
	{
		path: '/biology',
		name: 'biology',
		component: Biology
	},
	{
		path: '/chemistry',
		name: 'chemistry',
		component: Chemistry
	},
	{
		path: '/economics',
		name: 'economics',
		component: Economics
	},
	{
		path: '/physics',
		name: 'physics',
		component: Physics
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router;
