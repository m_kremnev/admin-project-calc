import { axios } from '@/axios';
import algebra from './algebra';

export default {
	namespaced: true,
	modules: {
		algebra
	}
};
