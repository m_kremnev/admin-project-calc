export default {
	namespaced: true,
	state: {
		dataGraf: {
			quadroGrafData: {
				width: 300,
				height: 300,
				padding: 30,
				rawData: [
					{ x: 0, y: 0 },
					{ x: 1, y: 1 },
					{ x: 2, y: 4 },
					{ x: 3, y: 9 },
					{ x: 4, y: 16 }
				],
				data: []
			},
			fracGrafData: {
				width: 300,
				height: 300,
				padding: 30,
				rawData: [
					{ x: 0, y: 3 },
					{ x: 1, y: 4 },
					{ x: 2, y: 5 },
					{ x: 3, y: 6 },
					{ x: 4, y: 7 }
				],
				data: []
			}
		},
		tableData: {
			quadraTableData: [
				{
					// eslint-disable-next-line
					name: `\{x}`,
					value1: 1.98,
					value2: 1.99,
					value3: 2.0,
					value4: 2.01,
					value5: 2.02
				},
				{
					// eslint-disable-next-line
					name: '{x^2}',
					value1: 3.92,
					value2: 3.96,
					value3: 4.0,
					value4: 4.04,
					value5: 4.08
				},
				{
					name: '\\mid{x^2}-{4}\\mid',
					value1: 0.08,
					value2: 0.04,
					value3: 0,
					value4: 0.04,
					value5: 0.08
				}
			],
			fracTableData: [
				{
					// eslint-disable-next-line
					name: `\{x}`,
					value1: 2.96,
					value2: 2.98,
					value3: 3.0,
					value4: 3.02,
					value5: 3.04
				},
				{
					// eslint-disable-next-line
					name: '{y}=\\dfrac{x^2 - 9}{x - 3}',
					value1: 5.96,
					value2: 5.98,
					value3: 'не определено',
					value4: 6.02,
					value5: 6.04
				},
				{
					name: '\\mid{f(x)}-{6}\\mid',
					value1: 0.04,
					value2: 0.02,
					value3: '',
					value4: 0.02,
					value5: 0.04
				}
			]
		}
	},
	mutations: {},
	actions: {},
	getters: {
		tableData(state) {
			return state.tableData;
		},
		grafData(state) {
			return state.dataGraf;
		}
	}
};
