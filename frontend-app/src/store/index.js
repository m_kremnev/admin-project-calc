import Vue from 'vue';
import Vuex from 'vuex';
import components from './components/index';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		components
	}
});
