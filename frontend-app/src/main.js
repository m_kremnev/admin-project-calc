import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import VueKatex from 'vue-katex';

//custom components

Vue.use(VueKatex);

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');

import './plugins/element.js';
import '@/assets/css/common.scss';
import 'katex/dist/katex.min.css';
import 'normalize.css';
