module.exports = {
	publicPath: '/',
	outputDir: 'dist',
	assetsDir: 'assets',
	indexPath: '../public',
	lintOnSave: 'warning',
	productionSourceMap: false
};
